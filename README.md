# 💻 Teste Full Stack PHP - Phidelis Tecnologia

[Layout](https://xd.adobe.com/view/5c3079dc-4d24-4d9c-8bb1-004437b6f107-0963/)

### Descrição Geral

O teste consiste no desenvolvimento de um site para uma agência de viagens onde o cliente (agência) terá uma area administrativa podendo assim cadastrar determinados itens tornando o site dinâmico. Disponibilizaremos o layout em Adobe XD para orientação visual do que se espera no resultado final. 

Desta forma serão avaliadas as habilidades do candidato na linguagem PHP observando lógica, domínio, padrões de projetos, organização, performance, velocidade na entrega, qualidade do código, análise e compreensão do escopo, assim como validar seus conhecimentos na integração entre interfaces front-end e back-end.

Após  finalização do teste realizar um pull request no repositório para validarmos o código.

### OBS: Para rodar o projeto é necessário instalar o node e instalar as dependências do gulp dentro do projeto e rodar o comando: "npm start" via terminal na raiz do projeto.


### Requisitos

- Arquitetura MVC
- Utilizar framework no back-end de sua preferência
- NÃO utilizar query buiders
- NÃO utilizar migrations
- Utilizar PHP 7.*
- PHP Orientado a Objetos
- [PSRs](https://www.php-fig.org/psr/)
- HTML5
- CCS3
- JavaScript
- Git
- MySQL

### 💡 Diferenciais

- Desenvolver framework próprio (Respeitando as regras de requisitos)
- Utilizar Template Engine
- Utilizar Design Patterns
- Clean Code
